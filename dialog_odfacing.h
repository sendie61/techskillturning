#ifndef DIALOG_ODFACING_H
#define DIALOG_ODFACING_H

#include <QDialog>

namespace Ui {
class Dialog_ODFacing;
}

class Dialog_ODFacing : public QDialog
{
    Q_OBJECT

public:
    explicit Dialog_ODFacing(QWidget *parent = nullptr);
    ~Dialog_ODFacing();

private:
    Ui::Dialog_ODFacing *ui;
};

#endif // DIALOG_ODFACING_H
