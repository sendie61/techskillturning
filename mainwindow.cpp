#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <dialog_odfacing.h>
#include "qevent.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    ui->listWidget->addItem("Face Front");
    ui->listWidget->addItem("Turn");
    ui->listWidget->installEventFilter(this);
}


MainWindow::~MainWindow()
{
    delete ui;
}

// Check for context clicks in listWidget and present context menu
bool MainWindow::eventFilter(QObject *source, QEvent *event)
{
    if (source->objectName().toStdString() == "listWidget")
    {
        if (event->type() == QEvent::ContextMenu && ui->listWidget->count()>0 )
        {
            ui->menu_Sequence->setDisabled(false);
            QMenu *menu= new QMenu();
            QContextMenuEvent *contextEvent = static_cast<QContextMenuEvent *>(event);

           // menu->addAction("Move");
            menu->addAction(ui->action_Move);
            menu->addAction(ui->action_Edit);
            menu->addAction(ui->action_Delete);
            menu->exec(contextEvent->globalPos());
            //QMessageBox::information(this, QString("source"),source->objectName());
            return true;
        }
        else
        {
            //Check here if any items of listWidget are selected, if not: Disable sequence menu-items
            if (ui->listWidget->count()==0)
            {
                ui->menu_Sequence->setDisabled(true);
            }
            return QMainWindow::eventFilter(source, event);
        }
   }
   return QMainWindow::eventFilter(source, event);
}



// ***** Actions *****
void MainWindow::on_actionE_xit_triggered()
{
    QMessageBox::StandardButton reply = QMessageBox::question(this, "Exit", "Do you really want to exit?",
                                                              QMessageBox::Yes | QMessageBox::No);
    if (reply == QMessageBox::Yes){
        QApplication::quit();
    }
}


void MainWindow::on_action_Move_triggered()
{
    QString s = ui->listWidget->currentItem()->text();
    QMessageBox::information(this, "Move", s);
}


void MainWindow::on_action_Edit_triggered()
{
    QString s = ui->listWidget->currentItem()->text();
    QMessageBox::information(this, "Edit", s);
}


void MainWindow::on_action_Delete_triggered()
{
    auto currentItem= ui->listWidget->currentItem();
    QString currentItemName= "Do you really want to delete '" + currentItem->text() +"'?";
//    QMessageBox::information(this, "Delete", s);
    QMessageBox::StandardButton reply = QMessageBox::question(this, "Delete", currentItemName,
                                                              QMessageBox::Yes | QMessageBox::No);
    if (reply == QMessageBox::Yes){
       // ui->listWidget->removeItemWidget(currentItem);
        delete ui->listWidget->takeItem(ui->listWidget->row(currentItem));
    }
}



void MainWindow::on_pushButton_ODTurning_clicked()
{
    // klik
}


// open OD-Facing dialog
void MainWindow::on_pushButton_ODFacing_clicked()
{
    Dialog_ODFacing dialog_odfacing;
    dialog_odfacing.setModal(true);
    dialog_odfacing.exec();

}




