#include "dialog_odfacing.h"
#include "ui_dialog_odfacing.h"
#include <QPixmap>

Dialog_ODFacing::Dialog_ODFacing(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Dialog_ODFacing)
{
    ui->setupUi(this);
    QPixmap picture = QPixmap(":/img/images/Turning_operations-1.png");
    ui->Pic_Label->setPixmap(picture.scaled(200,500,Qt::KeepAspectRatio));
}

Dialog_ODFacing::~Dialog_ODFacing()
{
    delete ui;
}
