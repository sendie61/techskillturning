#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QMessageBox>
#include <QMenu>


QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
    bool eventFilter(QObject *watched, QEvent *event);

private slots:
    void on_actionE_xit_triggered();

    void on_pushButton_ODTurning_clicked();

    void on_pushButton_ODFacing_clicked();

    void on_action_Move_triggered();

    void on_action_Edit_triggered();

    void on_action_Delete_triggered();

private:
    Ui::MainWindow *ui;
};
#endif // MAINWINDOW_H
